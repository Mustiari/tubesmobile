package com.example.acer.tubesmobile.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.acer.tubesmobile.R
import com.example.acer.tubesmobile.model.RealtimeDatabase
import kotlinx.android.synthetic.main.tambah_data.*
import java.util.*


class TambahDataActivity : AppCompatActivity() {

    private lateinit var inputRoutine: EditText
    private lateinit var inputDesc: EditText
    private lateinit var inputDate: TextView
    private lateinit var inputTime: TextView
    private lateinit var dateTime: Calendar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tambah_data)

        dateTime = Calendar.getInstance()

        inputRoutine = findViewById(R.id.inputNamaKegiatan)
        inputDesc = findViewById(R.id.inputDescKegiatan)
        inputDate = findViewById(R.id.inputTanggalKegiatan)
        inputTime = findViewById(R.id.inputWaktuKegiatan)

        val btnTambah = findViewById<Button>(R.id.btnAddDaily)
        val dt = DateTimePicker()
        inputDate.setOnClickListener {
            dt.datePicker(this, inputDate)
        }

        inputTime.setOnClickListener {
            dt.timePicker(this, inputTime)
        }

        btnTambah.setOnClickListener {
            getAllDataInput()
        }
    }

    private fun getAllDataInput(){
        val date = inputTanggalKegiatan.text.toString()
        val time = inputWaktuKegiatan.text.toString()
        val routine = inputNamaKegiatan.text.toString()
        val desc = inputDescKegiatan.text.toString()
        val tanggal = date.substring(5)
        val tanggalWaktu = tanggal.plus(" ").plus(time)
        val rd = RealtimeDatabase()
        rd.insertData(routine, desc, date, time, tanggalWaktu)

        startActivity(Intent(this, TampilDataActivity::class.java))
        finish()
    }

}
