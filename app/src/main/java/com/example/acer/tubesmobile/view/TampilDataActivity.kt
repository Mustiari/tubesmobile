package com.example.acer.tubesmobile.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.example.acer.tubesmobile.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.tampil_data.*
import android.widget.Button
import com.example.acer.tubesmobile.model.DailyRoutine
import com.example.acer.tubesmobile.model.RealtimeDatabase
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class TampilDataActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        updateData()
        setContentView(R.layout.tampil_data)

        val add = findViewById<Button>(R.id.addButton)
        val logout = findViewById<Button>(R.id.logoutButton)

        add.setOnClickListener {
            addData()
        }

        logout.setOnClickListener {
            logoutApp()
        }
    }

    private fun logoutApp() {
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun addData(){
        val intentAdd = Intent(this, TambahDataActivity::class.java)
        startActivity(intentAdd)
    }

    private fun updateData(){
        val ref = RealtimeDatabase().ref.orderByChild("dateTime")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.d("ambil data", p0.details)
            }

            override fun onDataChange(p0: DataSnapshot) {
                Log.d("ambil data", "berhasil mengambil data")
                val data = mutableListOf<DailyRoutine>()
                data.clear()
                for (daily in p0.children){
                    data.add(
                            DailyRoutine(
                                    daily.child("idDaily").getValue(String::class.java)!!,
                                    daily.child("daily").getValue(String::class.java)!!,
                                    daily.child("desc").getValue(String::class.java)!!,
                                    daily.child("date").getValue(String::class.java)!!,
                                    daily.child("time").getValue(String::class.java)!!,
                                    daily.child("dateTime").getValue(String::class.java)!!
                            )
                    )
                }
                updateList(data)
            }
        })
    }

    fun updateList(data: List<DailyRoutine>){
        listRoutine.layoutManager = LinearLayoutManager(this)
        listRoutine.adapter = DailyAdapter(this, data)
    }
}
