package com.example.acer.tubesmobile.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.TextView
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.acer.tubesmobile.R
import com.example.acer.tubesmobile.model.RealtimeDatabase
import java.util.*

class DetailDataActivity : AppCompatActivity() {
    private lateinit var routine: EditText
    private lateinit var desc: EditText
    private lateinit var datePicker: TextView
    private lateinit var timePicker: TextView
    private lateinit var dateTime: Calendar
    private lateinit var btnEdit: Button
    private lateinit var btnDelete: Button

    private lateinit var routineID: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_data)
        dateTime = Calendar.getInstance()
        routine = findViewById(R.id.inputDaily)
        desc = findViewById(R.id.inputDesc)
        datePicker = findViewById(R.id.dateRoutine)
        timePicker = findViewById(R.id.timeRoutine)
        btnEdit = findViewById(R.id.ubahButton)
        btnDelete = findViewById(R.id.hapusButton)
        setData()
        val dt = DateTimePicker()
        datePicker.setOnClickListener {
            dt.datePicker(this, datePicker)
        }
        timePicker.setOnClickListener {
            dt.timePicker(this, timePicker)
        }
        btnEdit.setOnClickListener {
            val date = datePicker.text.toString()
            val time = timePicker.text.toString()
            val tanggal = date.substring(5)
            val tanggalWaktu = tanggal.plus(" ").plus(time)
            RealtimeDatabase().updateData(routineID, routine.text.toString(), desc.text.toString(), date, time, tanggalWaktu)
            Toast.makeText(this, "Edit Routine Successfully", Toast.LENGTH_SHORT).show()
            nextActivity()
        }
        btnDelete.setOnClickListener {
            RealtimeDatabase().deleteData(routineID)
            nextActivity()
        }
    }

    private fun nextActivity(){
        startActivity(Intent(this, TampilDataActivity::class.java))
        finish()
    }

    private fun setData(){
        val edit = Editable.Factory.getInstance()
        routineID = intent.getStringExtra("idDaily")
        routine.text = edit.newEditable(intent.getStringExtra("daily"))
        desc.text = edit.newEditable(intent.getStringExtra("desc"))
        datePicker.text = intent.getStringExtra("date")
        timePicker.text = intent.getStringExtra("time")
    }
}
