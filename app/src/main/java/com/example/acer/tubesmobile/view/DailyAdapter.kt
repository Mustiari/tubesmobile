package com.example.acer.tubesmobile.view

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.acer.tubesmobile.R
import com.example.acer.tubesmobile.model.DailyRoutine
import kotlinx.android.synthetic.main.konten_recyclerview.view.*

class DailyAdapter(val context: Context, private val daily: List<DailyRoutine>): RecyclerView.Adapter<DailyAdapter.DailyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyViewHolder = DailyViewHolder(LayoutInflater.from(context).inflate(R.layout.konten_recyclerview, parent, false))

    override fun getItemCount()= daily.size

    override fun onBindViewHolder(holder: DailyViewHolder, position: Int) {
        holder.bindDailyRoutine(daily[position])
    }

    inner  class DailyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindDailyRoutine(listDaily: DailyRoutine){
            itemView.namaKegiatan.text = listDaily.daily
            itemView.desKegiatan.text = listDaily.desc
            itemView.tanggalKegiatan.text = listDaily.date
            itemView.waktuKegiatan.text = listDaily.time

            itemView.setOnClickListener {
                val intentDetail = Intent(context, DetailDataActivity::class.java)
                intentDetail.putExtra("idDaily", listDaily.idDaily)
                intentDetail.putExtra("daily", listDaily.daily)
                intentDetail.putExtra("desc", listDaily.desc)
                intentDetail.putExtra("date", listDaily.date)
                intentDetail.putExtra("time", listDaily.time)
                context.startActivity(intentDetail)
            }
        }
    }
}