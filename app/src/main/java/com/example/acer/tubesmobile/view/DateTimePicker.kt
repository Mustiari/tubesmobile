package com.example.acer.tubesmobile.view

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*

class DateTimePicker {
    private val dateTime = Calendar.getInstance()!!
    private val local = Locale("INDONESIAN")

    fun datePicker(context: Context, tanggal: TextView){
        val formatTanggal = SimpleDateFormat("EEEEEE, dd MMM yyyy", local)
        val date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            dateTime.set(Calendar.YEAR, year)
            dateTime.set(Calendar.MONTH, monthOfYear)
            dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            view.updateDate(year, monthOfYear, dayOfMonth)
            tanggal.text = formatTanggal.format(dateTime.time)

        }
        DatePickerDialog(context, date, dateTime.get(Calendar.YEAR), dateTime.get(Calendar.MONTH), dateTime.get(Calendar.DAY_OF_MONTH)).show()
    }

    fun timePicker(context: Context, waktu: TextView){
        val formatWaktu = SimpleDateFormat("HH:mm", local)
        val time = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            view.setIs24HourView(true)
            dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay)
            dateTime.set(Calendar.MINUTE, minute)
            waktu.text = formatWaktu.format(dateTime.time)
        }
        TimePickerDialog(context, time, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), true).show()
    }
}