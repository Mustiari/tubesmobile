package com.example.acer.tubesmobile.model

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.example.acer.tubesmobile.view.TampilDataActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class RealtimeDatabase {
    private val user = FirebaseAuth.getInstance().currentUser!!
    val ref = FirebaseDatabase.getInstance().reference.child(user.uid)

    fun insertData(routine: String, desc: String, date: String, time: String, dateTime: String){
        val pushID = ref.push().key
        val daily = DailyRoutine(pushID.toString(),routine, desc, date, time, dateTime)
        //add data to realtime database
        ref.child(pushID.toString()).setValue(daily)
                .addOnCompleteListener {
                    Log.d("tambah data", "Rutinitas Berhasil di tambahkan")
                }
                .addOnCanceledListener {
                    Log.d("tambah data", "Rutinitas Gagal di tambahkan")
                }
    }

    fun updateData(idDaily: String, routine: String, desc: String, date: String, time: String, dateTime: String){
        val daily = DailyRoutine(idDaily,routine, desc, date, time, dateTime)
        //add data to realtime database
        ref.child(idDaily).setValue(daily)
                .addOnCompleteListener {
                    Log.d("tambah data", "Rutinitas Berhasil di tambahkan")
                }
                .addOnCanceledListener {
                    Log.d("tambah data", "Rutinitas Gagal di tambahkan")
                }
    }

    fun deleteData(idDaily: String){
        ref.child(idDaily).removeValue()
                .addOnCompleteListener {
                    Log.d("delete data", "Success Deleted Daily")
                }
                .addOnCanceledListener {
                    Log.d("delete data", "Unsuccess Deleted Daily")
                }
    }
}