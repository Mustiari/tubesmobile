package com.example.acer.tubesmobile.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.example.acer.tubesmobile.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private lateinit var email: String
    private lateinit var password: String
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        auth = FirebaseAuth.getInstance()
        val btnLogin = findViewById<Button>(R.id.buttonLogin)
        btnLogin.setOnClickListener {
            email = inputEmail.text.toString()
            password = inputPassword.text.toString()
            loginAPP()
        }
    }

    private fun loginAPP() {
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = auth.currentUser
                        user?.let { updateUI(it) }
                    } else {
                        // If sign in fails, display a message to the user.
                        val tag = "fail"
                        Log.w(tag, "signInWithEmail:failure", task.exception)
                        registerUser()
                    }
                }
    }

    private fun registerUser(){
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = auth.currentUser
                        if (user != null) {
                            updateUI(user)
                        }
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(this, "Register Gagal $task", Toast.LENGTH_SHORT).show()
                    }
                }
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        currentUser?.let { updateUI(it) }
    }

    private fun updateUI(user: FirebaseUser){
        Log.w("login", "Login Berhasil")
        val intent = Intent(this, TampilDataActivity::class.java)
        intent.putExtra("uid", user.uid)
        startActivity(intent)
        finish()
    }

}