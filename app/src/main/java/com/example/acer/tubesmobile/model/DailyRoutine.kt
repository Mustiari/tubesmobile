package com.example.acer.tubesmobile.model

class DailyRoutine {
    var daily: String
    var desc: String
    var date: String
    var time: String
    var idDaily: String
    var dateTime: String

    constructor(){
        daily = ""
        desc = ""
        date = ""
        time = ""
        idDaily = ""
        dateTime = ""
    }

    constructor(idDaily: String, daily: String, desc: String, date: String, time: String, dateTime: String){
        this.idDaily = idDaily
        this.daily = daily
        this.desc = desc
        this.date = date
        this.time = time
        this.dateTime = dateTime
    }
}